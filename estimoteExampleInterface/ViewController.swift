//
//  ViewController.swift
//  estimoteExampleInterface
//
//  Created by Mateusz Kaczor on 07.03.2017.
//  Copyright © 2017 Mateusz Kaczor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Not so pretty solution...
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        //Cell size defined by cell content
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    //Some hardcode example data

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell: LAPointsCell = tableView.dequeueReusableCell(withIdentifier: "LAPointsCell") as! LAPointsCell
            
            
            cell.pointsLabel.text = "0"
            
            return cell
            
        } else if indexPath.row == 1 {
            
            let cell: LAHintCell = tableView.dequeueReusableCell(withIdentifier: "LAHintCell") as! LAHintCell
            
            return cell
            
        } else if indexPath.row == 2 {
            
            let cell: LATableSectionHeaderCell = tableView.dequeueReusableCell(withIdentifier: "LATableSectionHeaderCell") as! LATableSectionHeaderCell
            
            return cell
            
        } else {
            let cell: LAOfferCell = tableView.dequeueReusableCell(withIdentifier: "LAOfferCell") as! LAOfferCell
            
            return cell
        }
        
    
    }

}
