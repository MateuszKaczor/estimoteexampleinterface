//
//  CALayer+shadowColor.swift
//  estimoteExampleInterface
//
//  Created by Mateusz Kaczor on 07.03.2017.
//  Copyright © 2017 Mateusz Kaczor. All rights reserved.
//

import Foundation
import UIKit



/**
    Required to be able configure shadowColor from Interface Builder.
    In IB is used UIColor, but CALayer is using CGColor.
 */
extension CALayer {

    func setShadowColorIB(color: UIColor){
        shadowColor = color.cgColor
    }
    
    func setBorderColorIB(color: UIColor) {
        borderColor = color.cgColor
    }
}

