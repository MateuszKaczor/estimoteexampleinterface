//
//  LAPointsCell.swift
//  estimoteExampleInterface
//
//  Created by Mateusz Kaczor on 07.03.2017.
//  Copyright © 2017 Mateusz Kaczor. All rights reserved.
//

import UIKit

class LAPointsCell: UITableViewCell {
    
    
    @IBOutlet weak var pointsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
