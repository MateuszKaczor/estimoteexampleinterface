//
//  LAOfferCell.swift
//  estimoteExampleInterface
//
//  Created by Mateusz Kaczor on 07.03.2017.
//  Copyright © 2017 Mateusz Kaczor. All rights reserved.
//

import UIKit

class LAOfferCell: UITableViewCell {

    
    @IBOutlet weak var offerTitleLabel: UILabel!
    @IBOutlet weak var descryptionLabel: UILabel!
    @IBOutlet weak var requirePointLabel: UILabel!
    @IBOutlet weak var promotionButton: UIButton!
    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
